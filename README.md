# Vue.js project boilerplate

## Project setup
```
cp .env.example .env
npm install
cd .mock-now-server
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Run mock server (daemon) to get demo data.
```
npm run mock
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

