const ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sZW5kaW5nLXBsYWNlLmxvY1wvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NDQ3Nzk0MzYsImV4cCI6MTU0NTM4NDIzNiwibmJmIjoxNTQ0Nzc5NDM2LCJqdGkiOiJDSk1kQXNWUHcxRWhDdmNlIiwic3ViIjoxMDcsInBydiI6IjBmYWM3YjVmOGE5NjRlMTgxYjI0OGFmMjllZWJmN2Y1Yzg4ZTNmZjMiLCJ0eXBlIjoib3JnYW5pemF0aW9uIiwicm9sZSI6ImxlZ2FsX2ludmVzdG9yIiwib3JnYW5pemF0aW9uX2lkIjo2OH0.rJzStHb8fZfcClwqBrKyWYvv0q24rY2a-o1BX1fk61s';

const posts = [
  {
    "id": 1,
    "types": "posts",
    "attributes": {
      "id": 1,
      "title": "Why we moved from Angular 2 to Vue.js (and why we didn’t choose React)",
      "image": "https://cdn-images-1.medium.com/max/2000/1*PHmNXbvOfg5AHiMWWuaRXg.jpeg",
      "short_description": "At Rever (www.reverscore.com) we just released a new version of our web client using Vue.js. After 641 commits and 16 weeks of intense development here we are, very proud of a decision we took a while ago.",
      "body": "At Rever (www.reverscore.com) we just released a new version of our web client using Vue.js. After 641 commits and 16 weeks of intense development here we are, very proud of a decision we took a while ago.\n" +
        "\n" +
        "8 months ago our web client was using Angular 2. To be precise, it was using Angular 2 beta 9. This was a product written by an outsourcing company and we were never fully happy with it on many levels, from UX/UI to the architecture, and to some level, with Angular 2 itself.\n" +
        "\n" +
        "Before I continue, I admit that Angular 2 beta 9 is a different product than Angular 2.0, but that was exactly one of the problems. From beta9 to 2.0.0 there are 8 beta versions, 8 RC and the 2.0.0 version itself, 17 versions to upgrade in total. We did try to upgrade from beta 9 to 2.0.0 but too many things broke that made the upgrade non trivial. Also, about the same time we were questioning Angular 2 as our framework of choice, the Angular team decided to start working on Angular 4. While they promised it wouldn’t be too drastic, that meant that by the time we finished upgrading to Angular 2.0.0 we were going to need another upgrade. What a waste of time and limited resources.\n" +
        "\n" +
        "The main thing we didn’t like and we still don’t like about Angular 2 is Typescript. I know Angular 2 can be used with Javascript but again, the decision to use Typescript was already taken and from what I understand, using pure Javascript with Angular 2 is not the ideal way you should be using Angular 2. In any case, getting rid of Typescript meant a full rewrite of the project.\n" +
        "\n" +
        "I didn’t feel Typescript added substantial value and even worse, we noticed that our coding speed was reduced. With Typescript things that were really easy to do on Javascript like defining a simple object were more complicated to do on Typescript. I highly recommend you to read the following articles before you start using Typescript. It is not the right solution for everyone."
    },
  },
  {
    "id": 2,
    "types": "posts",
    "attributes": {
      "id": 2,
      "title": "Plans for the Next Iteration of Vue.js",
      "image": "https://cdn-images-1.medium.com/max/800/1*vFC8tDUGLlXIiqT7ymf3xg.png",
      "short_description": "Last week at Vue.js London I gave a brief sneak peek of what’s coming in the next major version of Vue. This post provides an in-depth overview of the plan.",
      "body": "Vue 2.0 was released exactly two years ago (how time flies!). During this period, the core has remained backwards compatible with five minor releases. We’ve accumulated a number of ideas that would bring improvements, but they were held off because they would result in breaking changes. At the same time, the JavaScript ecosystem and the language itself has been evolving rapidly. There are greatly improved tools that could enhance our workflow, and many new language features that could unlock simpler, more complete, and more efficient solutions to the problems Vue is trying to solve. What’s more exciting is that we are seeing ES2015 support becoming a baseline for all major evergreen browsers. Vue 3.0 aims to leverage these new language features to make Vue core smaller, faster, and more powerful.\n" +
        "\n" +
        "Vue 3.0 is currently in prototyping phase, and we have already implemented a runtime close to feature-parity with 2.x. Many of the items listed below are either already implemented, or confirmed to be feasible. Ones that are not yet implemented or still in exploration phase are marked with a *."
    },
  },
  {
    "id": 3,
    "types": "posts",
    "attributes": {
      "id": 3,
      "image": "https://placekitten.com/1000/300",
      "title": "What is Lorem Ipsum?",
      "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "body": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
  },
  {
    "id": 4,
    "types": "posts",
    "attributes": {
      "id": 4,
      "title": "Where does it come from?",
      "image": "https://placekitten.com/1000/300",
      "short_description": "Contrary to popular belief, Lorem Ipsum is not simply random text. ",
      "body": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n" +
        "\n" +
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."
    },
  },
  {
    "id": 5,
    "types": "posts",
    "attributes": {
      "id": 5,
      "title": "Why do we use it?",
      "image": "https://placekitten.com/1000/300",
      "short_description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
      "body": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
    },
  },
  {
    "id": 6,
    "types": "posts",
    "attributes": {
      "id": 6,
      "title": "Where can I get some?",
      "image": "https://placekitten.com/1000/300",
      "short_description": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      "body": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
    },
  },
];

module.exports = {
  "routes": {
    "/auth/login|POST": (request) => {
      const email = request.body.email;
      const password = request.body.password;

      if (email === 'test@gmail.com' && password === '123456') {
        return {
          "success": true,
          "data": {
            "access_token": ACCESS_TOKEN,
          },
        }
      }

      if (email.trim().length === 0) {
        return {
          statusCode: 422,

          "success": false,
          "errors": {
            "email": [
              "E-mail field is required."
            ],
          },
          "data": {
            "message": "Some data was invalid.",
          },
        }
      }

      if (password.trim().length === 0) {
        return {
          statusCode: 422,

          "success": false,
          "errors": {
            "password": [
              "Password field is required."
            ],
          },
          "data": {
            "message": "Some data was invalid.",
          },
        }
      }

      return {
        statusCode: 422,

        "success": false,
        "errors": {
          "email": [
            "These credentials do not match our records."
          ],
        },
        "data": {
          "message": "Some data was invalid.",
        },
      }
    },

    "/auth/logout|POST": (request) => {
      if (request.headers.authorization === `Bearer ${ACCESS_TOKEN}`) {
        return {
          "success": true,
          "data": {},
        }
      }

      return {
        statusCode: 401,

        "success": false,
        "data": {
          "message": "You must be logged in to perform this action.",
        },
      }
    },

    "/account|GET": (request) => {
      if (request.headers.authorization === `Bearer ${ACCESS_TOKEN}`) {
        return {
          "success": true,
          "data": {
            "type": "users",
            "id": 1,
            "attributes": {
              "id": 1,
              "first_name": "John",
              "last_name": "Doe",
              "email": "test@gmail.com",
            }
          },
        }
      }

      return {
        statusCode: 401,

        "success": false,
        "data": {
          "message": "You must be logged in to perform this action.",
        },
      }
    },

    "/posts|GET": (request) => {
      return {
        "success": true,
        "data": posts,
        "meta": {
          "current_page": 1,
          "from": 1,
          "last_page": 1,
          "path": "/posts",
          "per_page": 10,
          "to": 6,
          "total": 6
        }
      }
    },

    "/posts/:id|GET": (request) => {
      const id = parseInt(request.params.id);

      if (id < 1 || id > 6) {
        return {
          statusCode: 404,

          "success": false,
          "data": {
            "message": "Page not found.",
          },
        }
      }

      return {
        success: true,
        data: posts.filter(post => post.id === id)[0]
      };
    },
  }
};
