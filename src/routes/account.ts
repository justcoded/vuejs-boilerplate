import {RouteConfig} from 'vue-router';

const Account = () => import('@/views/account/Index.vue');

const Routes: () => RouteConfig[] = () => [
  {
    path: '/account',
    name: 'account.index',
    component: Account,
  },
];

export default Routes;
