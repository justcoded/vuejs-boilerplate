import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/home/Home.vue';

import AuthRoutes from '@/routes/auth';
import BlogRoutes from '@/routes/posts';
import AccountRoutes from '@/routes/account';

const NotFound = () => import('@/views/errors/404.vue');

Vue.use(Router);

const index = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/about/About.vue'),
    },

    ...AuthRoutes(),
    ...AccountRoutes(),
    ...BlogRoutes(),

    {
      path: '/not-found',
      name: '404',
      component: NotFound,
    },
  ],
});

export default index;
