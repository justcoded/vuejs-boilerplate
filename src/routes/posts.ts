import {RouteConfig} from 'vue-router';

const Index = () => import('@/views/posts/Index.vue');
const Show = () => import('@/views/posts/Show.vue');

const Routes: () => RouteConfig[] = () => [
  {
    path: '/blog',
    name: 'posts.index',
    component: Index,
  },
  {
    path: '/blog/:id',
    name: 'posts.show',
    component: Show,
  },
];

export default Routes;
