import {RouteConfig} from 'vue-router';

const Login = () => import('@/views/auth/Login.vue');
const Logout = () => import('@/views/auth/Logout.vue');

const Routes: () => RouteConfig[] = () => [
  {
    path: '/auth/login',
    name: 'auth.login',
    component: Login,
  },
  {
    path: '/auth/logout',
    name: 'auth.logout',
    component: Logout,
  },
];

export default Routes;
