export default {
  signedIn(state: any) {
    return Boolean(state.accessToken);
  },
};
