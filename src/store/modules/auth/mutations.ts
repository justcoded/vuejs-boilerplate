import Api from '@/api/Api';

export default {
  setAccessToken(state: any, token: string) {
    localStorage.setItem('accessToken', token);
    state.accessToken = token;

    Api.$instance.setAuthToken(token);
  },

  loadState(state: any) {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      state.accessToken = accessToken;
    }
  },
};
