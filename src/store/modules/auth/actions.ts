import {ActionTree} from 'vuex';
import {RootState} from '@/store/types';
import Auth from '@/api/Auth';

const actions: ActionTree<any, RootState> = {
  async logout({commit}) {
    const response = await Auth.logout();

    commit('setAccessToken', '');
  },
};

export default actions;
