import {ActionTree} from 'vuex';
import {RootState} from '@/store/types';
import UserApi from '@/api/Users';

const actions: ActionTree<any, RootState> = {
  async account({commit}) {
    commit('setLoading');

    const user = await UserApi.account();

    commit('setUser', user);
  },
};

export default actions;
