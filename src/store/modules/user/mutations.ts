import {Payload} from 'vuex';

export default {
  setLoading(state: any, bool: boolean = true) {
    state.loading = bool;
  },

  setUser(state: any, payload: Payload) {
    state.user = payload;
    state.loading = false;
  },
};
