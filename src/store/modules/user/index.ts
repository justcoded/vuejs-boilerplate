import state from './state';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import {Module} from 'vuex';
import {RootState} from '../../types';

const user: Module<any, RootState> = {
  state,
  actions,
  mutations,
  getters,
};

export default user;
