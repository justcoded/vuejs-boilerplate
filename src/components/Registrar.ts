import Vue from 'vue';
import Loader from '@/components/loader/Loader.vue';
import Pagination from '@/components/pagination/Pagination.vue';
import FormInput from '@/components/forms/inputs/FormInput.vue';
import TextInput from '@/components/forms/inputs/TextInput.vue';
import EmailInput from '@/components/forms/inputs/EmailInput.vue';
import PasswordInput from '@/components/forms/inputs/PasswordInput.vue';

export default class Registrar {
  public static register() {
    Vue.component('loader', Loader);
    Vue.component('pagination', Pagination);

    Vue.component('form-input', FormInput);
    Vue.component('form-input-text', TextInput);
    Vue.component('form-input-email', EmailInput);
    Vue.component('form-input-password', PasswordInput);
  }
}
