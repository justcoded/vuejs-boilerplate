import Axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import App from '@/app/App';

export default class Api {

  private static instance: Api;

  protected client: AxiosInstance;

  protected headers: any = {
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHTTPRequest',
  };

  private constructor() {
    this.client = this.initClient();
  }

  public setHeaders(headers: object): Api {
    this.headers = headers;

    this.initClient();

    return this;
  }

  public setHeader(header: string, value: string): Api {
    this.headers = {
      ...this.getHeaders(),
      ...{header: value},
    };

    this.initClient();

    return this;
  }

  public getHeaders(): object {
    return this.headers;
  }

  public getHeader(header: string): string {
    // @ts-ignore
    return this.headers.hasOwnProperty(header) ? this.headers[header] : null;
  }

  public setAuthToken(token: string): Api {
    if (token) {
      this.headers = {
        ...this.getHeaders(),
        ...{
          Authorization: `Bearer ${token}`,
        },
      };
    } else if (this.headers.hasOwnProperty('Authorization')) {
      delete this.headers.Authorization;
    }

    return this;
  }

  public async get(url: string, config?: AxiosRequestConfig): Promise<any> {
    this.initClient();
    return await this.client.get(url, config);
  }

  public async post(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
    this.initClient();
    return await this.client.post(url, data, config);
  }

  public static get $instance() {
    if (!Api.instance) {
      Api.instance = new Api();
    }

    return Api.instance;
  }

  protected initClient() {
    this.client = Axios.create({
      baseURL: process.env.VUE_APP_API_BASE_URL,
      headers: this.getHeaders(),
    });

    // Handle 404.
    this.client.interceptors.response.use(
      (response) => response,
      (error) => {
        if (error.response.status === 404) {
          App.$instance.$router.replace({name: '404'});
        } else {
          return Promise.reject(error);
        }
      },
    );

    return this.client;
  }
}
