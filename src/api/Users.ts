import Api from '@/api/Api';
import User from '@/models/User';

export const ACCOUNT_URL = 'account';

export default class Users {

  public static async account() {
    const response = await Api.$instance.get(ACCOUNT_URL);

    return User.make(response.data.data.attributes);
  }
}
