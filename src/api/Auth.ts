import Api from '@/api/Api';
import {LoginCredentials} from '@/api/index';

export const LOGIN_URL = 'auth/login';
export const LOGOUT_URL = 'auth/logout';

export default class Auth {

  public static async login(credentials: LoginCredentials) {
    try {
      return await Api.$instance.post(LOGIN_URL, credentials);
    } catch (e) {
      return e.response;
    }
  }

  public static async logout() {
    try {
      return await Api.$instance.post(LOGOUT_URL);
    } catch (e) {
      return e.response;
    }
  }
}
