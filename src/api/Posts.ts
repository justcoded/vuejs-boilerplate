import Api from '@/api/Api';
import Post from '@/models/Post';
import Resource from '@/api/Resource';

export const POSTS_URL = 'posts';
export const POST_URL = 'posts/{id}';

export default class Posts {

  public static async get(params: any) {
    const response = await Api.$instance.get(POSTS_URL, {params});

    return Resource.mapResponseToCollection(response, Post);
  }

  public static async find(id: any) {
    const response = await Api.$instance.get(POST_URL.replace('{id}', id));

    return response ? Post.make(response.data.data.attributes) : null;
  }
}
