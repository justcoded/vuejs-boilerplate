import {AxiosResponse} from 'axios';
import Collection from '@/models/Collection';

export default abstract class Resource {
  // @ts-ignore
  public static mapResponseToCollection(response: AxiosResponse, model) {
    // @ts-ignore
    const items = response.data.data.map((resource) => {
      return model.make(resource.attributes);
    });

    return Collection.make(items, response.data.meta);
  }
}
