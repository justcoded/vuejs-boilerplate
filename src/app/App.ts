import {Route} from 'vue-router';
import Api from '@/api/Api';
import {Application} from '@/app/types';
import {Vue} from 'vue-property-decorator';
import each from 'lodash/each';

export default class App {
  private static instance: Application;
  private static bus: Vue;

  public static create(app: Application, bus: Vue) {
    App.instance = app;
    App.bus = bus;
  }

  static get $instance(): Application {
    return App.instance;
  }

  static get $bus(): Vue {
    return App.bus;
  }

  public static setApiAccessToken(token?: string) {
    const accessToken = token || App.$instance.$store.state.auth.accessToken;
    Api.$instance.setAuthToken(accessToken);
  }

  public static init() {
    App.loadState();

    App.setApiAccessToken();

    App.$instance.$router.beforeEach((to: Route, from: Route, next: () => void) => {
      App.$instance.loading = true;
      next();
    });

    App.$instance.$router.afterEach((to: Route, from: Route) => {
      App.$instance.loading = false;
    });
  }

  protected static loadState() {
    // @ts-ignore
    each(App.$instance.$store._modules.root._children, (module, namespace) => {
      // @ts-ignore
      if (App.$instance.$store._mutations[`${namespace}/loadState`]) {
        module.context.commit('loadState');
      }
    });
  }
}
