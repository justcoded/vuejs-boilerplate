/// @OVERRIDABLE

import {Vue} from 'vue/types/vue';

export interface Application extends Vue {
  loading: boolean;
}
