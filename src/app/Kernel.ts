import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Component from 'vue-class-component';
import App from '@/app/App';
import AppView from '@/views/app/App.vue';
import GlobalComponentsRegistrar from '@/components/Registrar';
import router from '@/routes';
import store from '@/store';

import { library as fontsLibrary } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

export default class Kernel {

  public static async init(productionTip: boolean = true) {
    Vue.config.productionTip = productionTip;

    return await (new this())
      .registerPlugins()
      .registerComponentBasedRouterHooks()
      .registerGlobalVueComponents()
      .createAndInitApplication();
  }

  protected registerPlugins(): Kernel {
    Vue.use(BootstrapVue);

    fontsLibrary.add(faSpinner);
    // @ts-ignore
    Vue.component('fa', FontAwesomeIcon);

    return this;
  }

  protected registerComponentBasedRouterHooks(): Kernel {
    // Register the router hooks with their names.
    Component.registerHooks([
      'beforeRouteEnter',
      'beforeRouteLeave',
      'beforeRouteUpdate',
    ]);

    return this;
  }

  protected registerGlobalVueComponents(): Kernel {
    GlobalComponentsRegistrar.register();

    return this;
  }

  protected async createAndInitApplication() {
    App.create(
      new Vue({
        data: {loading: false},
        router,
        store,
        render: (h) => h(AppView),
      }).$mount('#app'),
      new Vue(),
    );

    return App.init();
  }
}
