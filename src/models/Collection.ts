import {CollectionMeta} from '@/models/types';

export default class Collection {
  protected collectionItems: any[];
  protected collectionMeta: CollectionMeta;

  constructor(items: any[], meta: any) {
    this.collectionItems = items;
    this.collectionMeta = this.parseMeta(meta);
  }

  public static make(items: any[], meta: any) {
    return new this(items, meta);
  }

  public get items() {
    return this.collectionItems;
  }

  public get meta() {
    return this.collectionMeta;
  }

  protected parseMeta(meta: any): CollectionMeta {
    return {
      ...meta,
      ...{
        currentPage: meta.current_page,
        lastPage: meta.last_page,
        perPage: meta.per_page,
      },
    };
  }
}
