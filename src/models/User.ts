import Model from '@/models/Model';

export default class User extends Model {
  public id!: number;
  public firstName!: string;
  public lastName!: string;
  public email!: string;

  public get name(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  protected map = {
    first_name: 'firstName',
    last_name: 'lastName',
  };
}
