export default abstract class Model {

  protected map: any;

  public static make(attributes: any[]) {
    // @ts-ignore
    // We create a fresh instance of model (child only) and fill it with attributes.
    const model = (new this());

    return model.setAttributes(attributes);
  }

  public setAttributes(attributes: any[]) {
    for (const attribute in attributes) {
      if (this.map && this.map[attribute]) {
        // @ts-ignore
        this[this.map[attribute]] = attributes[attribute];
      } else {
        // @ts-ignore
        this[attribute] = attributes[attribute];
      }
    }

    return this;
  }
}
