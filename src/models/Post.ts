import Model from '@/models/Model';

export default class Post extends Model {
  public id!: number;
  public title!: string;
  public image?: string;
  public shortDescription?: string;
  public body!: string;

  protected map = {
    short_description: 'shortDescription',
  };
}
