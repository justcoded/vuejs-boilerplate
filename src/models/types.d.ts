export interface CollectionMeta {
  currentPage: number;
  from: number;
  to: number;
  lastPage: number;
  path: string;
  perPage: number;
  total: number;
}
